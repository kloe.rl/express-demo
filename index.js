// importe EXPRESS
const express = require('express');
const app = express();
// defini le port qui sera écouté
const port = 3000;

// réponses de l'application en fonction des demandes adressées (chemins d'accès)
// l'aplication répondra "Cannot GET /..." si l'URL n'est pas un chemin d'accès valide
app.get('/no', (req, res) => {
    res.send('Ha det, og takk for all fisken!');
})

app.get('/en', (req, res) => {
    res.send('So long, and thanks for all the fish!');
})

app.get('/fr', (req, res) => {
    res.send('Salut, et encore merci pour le poisson!');
})

app.get('/cat-database', (req, res) => {
    res.send([{ name: 'Vévé', age: 14, lifes: 6 }, { name: 'Psss', age: 2, lifes: 9 }, { name: 'Crépi', age: 8, lifes: 3 }]);
})

// l'application écoute le port 3000 à la recherche de connexions
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})